package org.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */

public class SumPairsAppTest {

    @Test
    public void shouldAnswerWithTwoTest1() {
        Integer k = 6;
        Integer[] values = {2, 1, 4, 5, 3};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithTwoTest2() {
        Integer k = 6;
        Integer[] values = {3, 1, 4, 5, 2};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithTwoTest3() {
        Integer k = 6;
        Integer[] values = {3, 4, 5, 1, 2};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithTwoTest4() {
        Integer k = 6;
        Integer[] values = {-3, 4, 5, 1, 9};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithZeroTest1() {
        Integer k = -6;
        Integer[] values = {-3, 4, -12, -6, 9};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 0;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithOneTest1() {
        Integer k = -6;
        Integer[] values = {-3, 4, -12, 6, 9};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithOneTest2() {
        Integer k = 0;
        Integer[] values = {-3, 4, -6, 6, 9};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithTwoTest6() {
        Integer k = 6;
        Integer[] values = {-3, 4, 6, 0, 9, 12, -46666, 76, 54};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithMaxTest1() {
        Integer k = Integer.MAX_VALUE;
        Integer[] values = {-3, 4, Integer.MAX_VALUE, 0, 9, 12, -46666, 76, 54};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithMaxTest2() {
        Integer k = 0;
        Integer[] values = {-3, -2147483647, Integer.MAX_VALUE, 0, 9, 12, -46666, 76, 54};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAnswerWithMinTest1() {
        Integer k = 0;
        Integer[] values = {-3, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 9, 12, -46666, 76, 54};

        Integer actual = SumPairsApp.countAllPairsByGivenSum(values, k);
        Integer expected = 0;

        assertEquals(expected, actual);
    }
}
