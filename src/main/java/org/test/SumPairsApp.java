package org.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * You are given an array of n unique integers a = a[0], a[1], ... , a[n - 1] and an integer value k. Find and print the number of pairs (a[i], a
 * [j]) where i < j and a[i] + a[j] = k.
 */
public class SumPairsApp {

    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        Set<Integer> valuesSet = new LinkedHashSet<>();
        String line = stdin.readLine().trim();
        Integer k = null;
        if (isNumeric(line)) {
            k = Integer.valueOf(line);
        } else {
            System.exit(0);
        }

        line = stdin.readLine().trim();
        while (isNumeric(line)) {
            Integer value = Integer.valueOf(line);
            if (valuesSet.contains(value)) {
                System.out.println("All array values should be unique!");
                System.exit(0);
            }
            valuesSet.add(value);
            line = stdin.readLine().trim();
        }
        System.out.println(countAllPairsByGivenSum(valuesSet.toArray(new Integer[0]), k));
    }

    /**
     * You are given an array of n unique integers a = a[0], a[1], ... , a[n - 1] and an integer value k. Find and print the number of pairs (a[i], a
     * [j]) where i < j and a[i] + a[j] = k.
     * Input:
     *  The values k, a[0], a[1], ... a[n - 1], one value per line. Empty lines should be ignored.
     * Output:
     *  Print the number of pairs matching the criteria.
     * Constraints:
     *  All values are 32-bit signed integers
     *  2 <= n < 10e6
     * Example:
     *  Given k = 6 and a = [2, 1, 4, 5, 3]. The pairs matching the criteria are (2, 4) and (1, 5).
     *
     * Solution:
     *  Create a Hash Set setOfPairs
     *  Run a loop and scan over the array values[] for each values[i]. Check if sum-values[i] is present in the hash set or not.
     *  If yes, we have found the pair increase counter.
     *  If no, then insert values[i] into the Hash Set
     */
    public static Integer countAllPairsByGivenSum(Integer[] values, Integer sum) {
        if (values.length < 2) {
            return 0;
        }
        int numberOfPairs = 0;

        Set<Integer> setOfPairs = new HashSet<>();

        for (Integer value : values) {
            Integer pair = sum - value;
            if (setOfPairs.contains(pair)) {
                numberOfPairs++;
            } else {
                setOfPairs.add(value);
            }
        }

        return numberOfPairs;
    }

    private static boolean isNumeric(String strNum) {
        if (strNum == null || strNum.isEmpty()) {
            return false;
        }
        try {
            Integer i = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            System.out.println("Wrong input!");
            return false;
        }
        return true;
    }
}
