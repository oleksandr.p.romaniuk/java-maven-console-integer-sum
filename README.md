Given an array of n unique integers a = a[0], a[1], ... , a[n - 1] and an integer value k. 
    Find and print the number of pairs (a[i], a[j]) where i < j and a[i] + a[j] = k.
Input
    The values k, a[0], a[1], ... a[n - 1], one value per line. Empty lines should be ignored.
Output
    Print the number of pairs matching the criteria.
Constraints
    All values are 32-bit signed integers
    2 <= n < 10e6
Example
    Given k = 6 and a = [2, 1, 4, 5, 3]. The pairs matching the criteria are (2, 4) and (1, 5).
Input
6
2
1
4
5
3

The expected output is:
Output
2

## Tools
Following are the tools needed to build and run the project:
- Maven

## How to run
Following are the commands that are needed to run from project root directory

    $ mvn clean package
    $ cd /target
    $ java -jar sumpair-1.0.jar

## Test
For running test following is the command:

    $ mvn test